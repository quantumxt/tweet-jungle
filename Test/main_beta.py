#Use python3
#In this code, the serial I2c LCD Display was used
import http.client, urllib.parse, json	#For http request
import os

import smbus	#I2C LCD

import time
import RPi.GPIO as GPIO
from twython import TwythonStreamer

ttsHost = "https://speech.platform.bing.com"


def getSpeech():
	#-------------------HTTP REQUEST---------------------------
	#Note: Sign up at http://www.projectoxford.ai to get a subscription key.  
	#Search for Speech APIs from Azure Marketplace.
	#Use the subscription key as Client secret below.
	clientId = "nothing"
	clientSecret = "6e2afb5e05c6446fb63fe32da1bbe058"

	params = urllib.parse.urlencode({'grant_type': 'client_credentials', 'client_id': clientId, 'client_secret': clientSecret, 'scope': ttsHost})

	print ("The body data: %s" %(params))

	headers = {"Content-type": "application/x-www-form-urlencoded"}
				
	AccessTokenHost = "oxford-speech.cloudapp.net"
	path = "/token/issueToken"

	# Connect to server to get the Oxford Access Token
	conn = http.client.HTTPSConnection(AccessTokenHost)
	conn.request("POST", path, params, headers)
	response = conn.getresponse()
	print(response.status, response.reason)

	data = response.read()
	print(data)
	conn.close()
	accesstoken = data.decode("UTF-8")
	print ("Oxford Access Token: " + accesstoken)

	#decode the object from json
	ddata=json.loads(accesstoken)
	access_token = ddata['access_token']

	#Get message
	text = input('Enter a message to be read: ')

	#Body of request
	body = "<speak version='1.0' xml:lang='en-us'><voice xml:lang='en-us' xml:gender='Female' name='Microsoft Server Speech Text to Speech Voice (en-US, ZiraRUS)'>"+text+"</voice></speak>"

	headers = {"Content-type": "application/ssml+xml", 
				"X-Microsoft-OutputFormat": "riff-16khz-16bit-mono-pcm", 
				"Authorization": "Bearer " + access_token, 
				"X-Search-AppId": "07D3234E49CE426DAA29772419F436CA", 
				"X-Search-ClientID": "1ECFAE91408841A480F00935DC390960", 
				"User-Agent": "TTSForPython"}
				
	#Connect to server to synthesize the wave
	conn = http.client.HTTPSConnection("speech.platform.bing.com:443")
	conn.request("POST", "/synthesize", body, headers)
	response = conn.getresponse()
	print(response.status, response.reason)
	data = response.read()
	f = open('speech.wav','wb')
	f.write(data)
	f.close()
	conn.close()
	print("The synthesized wave length: %d" %(len(data)))

	#Play audio
	os.system('aplay speech.wav')

#Check internet connection
def check_connectivity(reference):
    try:
        urllib.request.urlopen(reference, timeout=1)
        return True
    except urllib.request.URLError:
        return False
	
#---------------------------------------------------------------
#---I2C LCD ---------------------------------

# Define some device parameters
I2C_ADDR  = 0x27 # I2C device address
LCD_WIDTH = 16   # Maximum characters per line

# Define some device constants
LCD_CHR = 1 # Mode - Sending command
LCD_CMD = 0 # Mode - Sending data

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
LCD_LINE_3 = 0x94 # LCD RAM address for the 3rd line
LCD_LINE_4 = 0xD4 # LCD RAM address for the 4th line

LCD_BACKLIGHT  = 0x08  # On
#LCD_BACKLIGHT = 0x00  # Off

ENABLE = 0b00000100 # Enable bit

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005

#Open I2C interface
#bus = smbus.SMBus(0)  # Rev 1 Pi uses 0
bus = smbus.SMBus(1) # Rev 2 Pi uses 1

def lcd_init():
  # Initialise display
  lcd_byte(0x33,LCD_CMD) # 110011 Initialise
  lcd_byte(0x32,LCD_CMD) # 110010 Initialise
  lcd_byte(0x06,LCD_CMD) # 000110 Cursor move direction
  lcd_byte(0x0C,LCD_CMD) # 001100 Display On,Cursor Off, Blink Off 
  lcd_byte(0x28,LCD_CMD) # 101000 Data length, number of lines, font size
  lcd_byte(0x01,LCD_CMD) # 000001 Clear display
  time.sleep(E_DELAY)

def lcd_byte(bits, mode):
  # Send byte to data pins
  # bits = the data
  # mode = 1 for character
  #        0 for command

  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT
  bits_low = mode | ((bits<<4) & 0xF0) | LCD_BACKLIGHT

  # High bits
  bus.write_byte(I2C_ADDR, bits_high)
  lcd_toggle_enable(bits_high)

  # Low bits
  bus.write_byte(I2C_ADDR, bits_low)
  lcd_toggle_enable(bits_low)

def lcd_toggle_enable(bits):
  # Toggle enable
  time.sleep(E_DELAY)
  bus.write_byte(I2C_ADDR, (bits | ENABLE))
  time.sleep(E_PULSE)
  bus.write_byte(I2C_ADDR,(bits & ~ENABLE))
  time.sleep(E_DELAY)

def lcd_string(message,line):
  # Send string to display

  message = message.ljust(LCD_WIDTH," ")

  lcd_byte(line, LCD_CMD)

  for i in range(LCD_WIDTH):
    lcd_byte(ord(message[i]),LCD_CHR)

def main():
  # Main program block

  # Initialise display
  lcd_init()

  # Send some test
  lcd_string(str(check_connectivity(ttsHost)),LCD_LINE_1)
  time.sleep(5)

  while True:

  
    # Send some test
    lcd_string("RPiSpy         <",LCD_LINE_1)
    lcd_string(uname -a,LCD_LINE_2)

    time.sleep(3)
  
    # Send some more text
    lcd_string(">         RPiSpy",LCD_LINE_1)
    lcd_string(">        I2C LCD",LCD_LINE_2)

    time.sleep(3)

if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    lcd_byte(0x01, LCD_CMD)
